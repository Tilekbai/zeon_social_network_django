from typing import Any
from django.db import models
from django.shortcuts import (
    render,
    redirect,
    get_object_or_404,
)
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (
    ListView,
    DetailView,
    DeleteView,
)
from .models import (
    Profile,
    Dweet,
    UploadImage,
)
from .forms import (
    DweetForm,
    UserImageForm,
)


def dashboard(request):
    if request.user.is_authenticated:
        form = DweetForm(request.POST or None)
        if request.method == "POST":
            if form.is_valid():
                dweet = form.save(commit=False)
                dweet.user = request.user
                dweet.save()
                return redirect("dwitter:dashboard")
            
        followed_dweets = Dweet.objects.filter(
            user__profile__in=request.user.profile.follows.all()
        ).order_by("-created_at")

        return render(
            request,
            "dwitter/dashboard.html",
            {"form": form, "dweets": followed_dweets},
        )
    else:
        return redirect("login")

def profile_list(request):
    profiles = Profile.objects.exclude(user=request.user)
    return render(request, "dwitter/profile_list.html", {"profiles": profiles})

def profile(request, pk):
    if not hasattr(request.user, 'profile'):
        missing_profile = Profile(user=request.user)
        missing_profile.save()

    profile = Profile.objects.get(pk=pk)
    if request.method == "POST":
        current_user_profile = request.user.profile
        data = request.POST
        action = data.get("follow")
        if action == "follow":
            current_user_profile.follows.add(profile)
        elif action == "unfollow":
            current_user_profile.follows.remove(profile)
        current_user_profile.save()
    return render(request, "dwitter/profile.html", {"profile": profile})

def image_request(request):  
    if request.method == 'POST': 
        form = UserImageForm(request.POST, request.FILES, user=request.user)  
        if form.is_valid():  
            uploadimage = form.save(commit=False)
            uploadimage.profile = request.user.profile
            uploadimage.save()   
            img_object = form.instance                
            return render(request, 'image_form.html', {'form': form, 'img_obj': img_object}) 
    else:  
        form = UserImageForm(user=request.user)  
  
    return render(request, 'image_form.html', {'form': form})

class ImageListView(ListView):
    model = UploadImage
    template_name = "image/image_list.html"
    context_object_name = "my_images"

    def get_queryset(self):
        queryset = UploadImage.objects.filter(profile=self.kwargs["pk"])
        return queryset

class ImageDetailView(DetailView):
    model = UploadImage
    template_name = "image/image_detail.html"
    context_object_name = "my_image"

    def get_queryset(self):
        queryset = UploadImage.objects.all().filter(pk=self.kwargs["pk"])   
        return queryset

class ImageDeleteView(LoginRequiredMixin, DeleteView):
    model = UploadImage
    context_object_name = "my_image"
    template_name = "image/image_confirm_delete.html"
    success_url = reverse_lazy("dwitter:dashboard")