from typing import Any
from django import forms
from .models import (
    Dweet,
    UploadImage,
)

class DweetForm(forms.ModelForm):
    body = forms.CharField(required=True)
    widget=forms.widgets.Textarea(
        attrs={
            "placeholder": "Dweet something...",
            "class": "textarea is-success is-medium",
        }
    ),
    label="",

    class Meta:
        model = Dweet
        exclude = ("user", )
  
class UserImageForm(forms.ModelForm):  
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')  
        super(UserImageForm, self).__init__(*args, **kwargs)

    class Meta: 
        model = UploadImage  
        exclude = ("profile", )


    def save(self, commit: bool = True) -> Any:
        instance = super(UserImageForm, self).save(commit=False)
        instance.profile = self.user.profile
        if commit:
            instance.save()
        return instance