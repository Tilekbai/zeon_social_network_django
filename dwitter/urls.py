from django.urls import (
    path,
    include,
)
from .views import (
    dashboard,
    profile_list,
    profile,
    image_request,
    ImageListView,
    ImageDetailView,
    ImageDeleteView,
)

app_name = "dwitter"

urlpatterns = [
    path("", dashboard, name="dashboard"),
    path("accounts/", include("django.contrib.auth.urls")),
    path("profile_list", profile_list, name="profile_list"),
    path("profile/<int:pk>", profile, name="profile"),
    path('add_pic', image_request, name = "image_add"),
    path('gellery/<int:pk>', ImageListView.as_view(), name = "image_all"),
    path("image_detail/<int:pk>", ImageDetailView.as_view(), name="image_detail"),
    path("image_delete/<int:pk>", ImageDeleteView.as_view(), name="image_delete"),
]