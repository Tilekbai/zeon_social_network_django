from django.contrib import admin
from django.contrib.auth.models import (
    Group,
    User,
)
from .models import (
    Profile,
    Dweet,
    UploadImage,
)


class ProfileInline(admin.StackedInline):
    model = Profile

class UserAdmin(admin.ModelAdmin):
    model = User
    fields = ["username"]
    inlines = [ProfileInline]

admin.site.register(Dweet)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.unregister(Group)

class UploadImageAdmin(admin.ModelAdmin):
    model = UploadImage
    fields = ["profile", "caption", "image"]
    
admin.site.register(UploadImage, UploadImageAdmin)